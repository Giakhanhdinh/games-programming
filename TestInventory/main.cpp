//
//  main.cpp
//  TestInventory
//
//  Created by Khanh Dinh on 9/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include <iostream>
#include "Player.hpp"
#include "Item.hpp"
#include "Inventory.hpp"
using namespace std;

int main(int argc, const char * argv[])
{
    Player p;
    Inventory ofPlayer = p.getInventory();
    ofPlayer.addItem("computer");
    ofPlayer.addItem("sword");
    ofPlayer.addItem("bowl");
    ofPlayer.Print();
    return 0;
}
