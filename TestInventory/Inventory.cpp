//
//  Inventory.cpp
//  TestInventory
//
//  Created by Khanh Dinh on 9/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Inventory.hpp"
#include <iostream>
using namespace std;

void Inventory::addItem(string name)
{
    Item* toNewItem = new Item(name);
    items.push_back(toNewItem);
}

Item* Inventory::fetchItem(string name)
{
    for (int i = 0; i < items.size(); i++)
        if (items[i]->getName() == name)
            return items[i];
    return NULL;
}

void Inventory::removeItem(string name)
{
    Item* item = fetchItem(name);
    vector<Item*>::iterator position = find(items.begin(), items.end(), item);
    if (item != NULL)
        items.erase(position);
}

void Inventory::Print()
{
    for (int i = 0; i < items.size(); i++)
        cout << items[i]->getName() << endl;
}
