//
//  Inventory.hpp
//  TestInventory
//
//  Created by Khanh Dinh on 9/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Inventory_hpp
#define Inventory_hpp

#include <stdio.h>
#include "Item.hpp"
#include <string>
#include <vector>
using namespace std;

class Inventory
{
private:
    vector<Item*> items;
public:
    void addItem(string name);
    Item* fetchItem(string name);
    void removeItem(string name);
    void Print();
};

#endif /* Inventory_hpp */
