//
//  Player.hpp
//  TestInventory
//
//  Created by Khanh Dinh on 9/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include "Inventory.hpp"

class Player
{
private:
    Inventory _inventory;
public:
    Inventory getInventory();
    
};
#endif /* Player_hpp */
