//
//  Item.hpp
//  TestInventory
//
//  Created by Khanh Dinh on 9/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Item_hpp
#define Item_hpp

#include <stdio.h>
#include <string>
using namespace std;

class Item
{
private:
    string _name;
public:
    Item(string name);
    string getName();
};

#endif /* Item_hpp */
