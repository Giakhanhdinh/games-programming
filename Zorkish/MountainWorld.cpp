//
//  MountainWorld.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "MountainWorld.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void MountainWorld::DisplayState()
{
    cout << endl << endl;
    cout << "Zorkish :: Mountain World" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Input your command" << endl;
}

void MountainWorld::HandleUserInput()
{
    string command;
    //cin.ignore();
    cin.clear();
    getline(cin, command);
    while (command == "")
    {
        cout << "Input your command" << endl;
        getline(cin, command);
    }
    _cmdmanager->Process(command);
//    if (command == "hiscore")
//        GameStateManager::SwitchToHallOfFame();
//    if (command == "quit")
//        GameStateManager::SwitchToMainMenu();
//    else
//    {
//        lookCommand->Process(_myWorld, command);
//        cout << _myWorld->PlayerLocation();
//    }
}

MountainWorld* MountainWorld::reset()
{
    MountainWorld* newAdventure = new MountainWorld();
    return newAdventure;
}

MountainWorld* MountainWorld::_instance = NULL;
MountainWorld::MountainWorld()
{
}

void MountainWorld::Load(string fileName)
{
    string line;
    ifstream myFile;
    myFile.open(fileName);
   
    while (getline(myFile, line))
    {
        vector<string> attributes = SplitStringOnce(line, ":");
        if (attributes[0] == "S")
            _myWorld->AddPlayer(attributes[1]);
        else if (attributes[0] == "L")
            _myWorld->AddLocation(attributes[1]);
        else if (attributes[0] == "I")
            _myWorld->AddItem(attributes[1]);
    }
    myFile.close();
}

