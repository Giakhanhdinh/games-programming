//
//  Command.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/13/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Command.hpp"
Command::Command(World* world)
{
    _world = world;
}

World* Command::GetWorld()
{
    return _world;
}


