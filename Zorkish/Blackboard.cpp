//
//  Blackboard.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/31/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Blackboard.hpp"
Blackboard* Blackboard::board;
Blackboard* Blackboard::GetInstance()
{
    if (board == NULL)
        board = new Blackboard();
    return board;
}

void Blackboard::onMessage(Message *msg)
{
    if (msg->GetType() == "write")
        cout << msg->GetData() << endl;
}

Blackboard::Blackboard()
{
}
