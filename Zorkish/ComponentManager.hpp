//
//  ComponentManager.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/5/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef ComponentManager_hpp
#define ComponentManager_hpp

#include <iostream>
#include "Health.hpp"
#include "Damage.hpp"
#include "CanContain.hpp"
#include "CanBePickedUp.hpp"
using namespace std;

class ComponentManager
{
private:
    Health* _health = NULL;
    Damage* _damage = NULL;
    CanContain* _cancontain = NULL;
    CanBePickedUp* _canbepickedup = NULL;
public:
    Health* GetHealth();
    Damage* GetDamage();
    CanContain* GetCanContain();
    CanBePickedUp* GetCanBePickedUp();
    void InitiateHealth(int count);
    void InitiateDamage(int count);
    void InitiateCanContain();
    void InitiateCanBePickedUp();
    void RegisterComponent(string component);
};

#endif /* ComponentManager_hpp */
