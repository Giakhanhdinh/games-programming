//
//  About.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "About.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void About::DisplayState()
{
    GameStateManager::ClearScreen();
    vector<string> toDisplay = LoadFile("about.txt");
    for (int i = 0; i < toDisplay.size(); i++)
        cout << toDisplay[i] << endl;
}

void About::HandleUserInput()
{
    char command;
    cin.ignore();
    cin.clear();
    command = cin.get();
    while (command != 27)
    {
        cout << "Press ESC or Enter to return to the Main Menu" << endl;
        command = cin.get();
    }
    GameStateManager::SwitchToMainMenu();
}

About* About::_instance = NULL;
About* About::Instance()
{
    if (_instance == NULL)
        _instance = new About();
    return _instance;
}

About::About()
{
    
}
