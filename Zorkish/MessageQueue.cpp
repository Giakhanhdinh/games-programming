//
//  MessageQueue.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/10/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "MessageQueue.hpp"

queue<Message*> MessageQueue::messages;
stack<Message*> MessageQueue::history;
void MessageQueue::Dispatch()
{
    while (!messages.empty())
    {
        Message* message = messages.front();
        messages.pop();
        Subscriber* to = message->GetRecipient();
        to->onMessage(message);
    }
}

void MessageQueue::AddMessage(Message *newmsg)
{
    messages.push(newmsg);
    history.push(newmsg);
    Dispatch();
}

MessageQueue::MessageQueue()
{
}

void MessageQueue::ReverseMessage(Message* msg)
{
    messages.push(msg);
    Dispatch();
}

Message* MessageQueue::PopHistory()
{
    Message* lastMess = history.top();
    history.pop();
    return lastMess;
}

