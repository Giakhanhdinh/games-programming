//
//  WaterWorld.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef WaterWorld_hpp
#define WaterWorld_hpp

#include <stdio.h>
#include "GameState.hpp"
using namespace std;

class WaterWorld : public GameState
{
public:
    void DisplayState();
    void HandleUserInput();
    static WaterWorld* reset();
private:
    static WaterWorld* _instance;
protected:
    WaterWorld();
};

#endif /* WaterWorld_hpp */
