//
//  WaterWorld.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "WaterWorld.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void WaterWorld::DisplayState()
{
    GameStateManager::ClearScreen();
    cout << "Zorkish :: Water World" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Input your command" << endl;
}

void WaterWorld::HandleUserInput()
{
    string command;
    cin.ignore();
    cin.clear();
    getline(cin, command);
    while (command != "hiscore" && command != "quit")
    {
        cout << "Input your command" << endl;
        getline(cin, command);
    }
    if (command == "hiscore")
        GameStateManager::SwitchToHallOfFame();
    if (command == "quit")
        GameStateManager::SwitchToMainMenu();
}

WaterWorld* WaterWorld::_instance = NULL;
WaterWorld* WaterWorld::reset()
{
    _instance = new WaterWorld();
    return _instance;
}

WaterWorld::WaterWorld()
{
    
}
