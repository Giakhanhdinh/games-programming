//
//  About.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//
#ifndef About_hpp
#define About_hpp

#include <iostream>
#include "GameState.hpp"
#include "FileOperation.hpp"
using namespace std;

class About : public GameState
{
    public:
        void DisplayState();
        void HandleUserInput();
    static About* Instance();
private:
    static About* _instance;
protected:
    About();
};

#endif /* About_hpp */
