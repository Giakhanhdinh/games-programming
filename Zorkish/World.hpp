//
//  World.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef World_hpp
#define World_hpp

#include <iostream>
#include <string>
#include <map>
#include "Location.hpp"
#include "Player.hpp"
#include "Item.hpp"

using namespace std;

class World
{
private:
    map<int, Location*> _rooms;
    map<int, Item*> _items;
    Player* _player = NULL;
    static World* _instance;
    World();
public:
    static World* NewWorld();
    void AddLocation(string fromFile);
    void AddPlayer(string fromFile);
    int MovePlayerTo(string dir);
    string PlayerLocation();
    string ItemsOntheFloor();
    Player* GetPlayer();
    Location* GetLocationAtId(int id);
    Location* GetPlayerLocation();
    Item* GetItemAtId(int id);
    string ItemDesc(int itemId);
    void AddItem(string fromFile);
    Item* FindItemWithName(string name);
    Item* FindItemWithNameInCurrentLoc(string name);
    Item* FindItemWithNameInPlayer(string name);
    Item* FindItemWithNameInAnotherItem(string name, Item* item);
    vector<Item*> GetItemsInsideAnItem(Item* item);
    void AddItemToPlayer(int id);
    Item* RemoveItemFromPlayer(string name);
    Item* RemoveItemFromCurrentLoc(string name);
    void AddItemToCurrentLoc(int id);
    void RemoveItemFromContainer(Item* container, Item* containedItem);
    void AddItemToContainer(Item* container, Item* containedItem);
};

#endif /* World_hpp */
