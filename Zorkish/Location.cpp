//
//  Location.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Location.hpp"
void Location::InitDirection()
{
    _direction["n"] = "north";
    _direction["w"] = "west";
    _direction["s"] = "south";
    _direction["e"] = "east";
    _direction["ne"] = "northeast";
    _direction["se"] = "southeast";
    _direction["nw"] = "northwest";
    _direction["sw"] = "southwest";
    _direction["u"] = "up";
    _direction["d"] = "down";
}

Location::Location(string fromFile)
{
    InitDirection();
    CreateLocation(fromFile);
}

string Location::Direction(string key)
{
    if (_direction.count(key) != 0)
        return _direction[key];
    return key;
}

void Location::CreateLocation(string fromFile)
{
    vector<string> attributes = SplitString(fromFile, ":");
    vector<string> connections = SplitString(attributes[3], ","); //get the rooms adjacent and their directions
    _id = stoi(attributes[0]);
    _name = attributes[1];
    _desc = attributes[2];
    for (int i = 0; i < connections.size(); i++)
    {
        vector<string> dirvsroomid = SplitString(connections[i], "=");
        _adjacents[Direction(dirvsroomid[0])] = stoi(dirvsroomid[1]);
    }
    
    if (attributes.size() > 4)
    {
        vector<string> itemsID = SplitString(attributes[4], ",");
        for (int i = 0; i < itemsID.size(); i++)
            _items.push_back(stoi(itemsID[i]));
    }
}

int Location::GetAdjacent(string direction)
{
    if (_adjacents.count(Direction(direction)) != 0)
        return _adjacents[Direction(direction)];
    return -1;
}

string Location::AdjacentDirections()
{
    string result = "";
    if (_adjacents.size() == 0)
        return "You are stuck";
    else
    {
        result += "You have exits to the ";
        map<string, int>::iterator it;
        for (it = _adjacents.begin(); it != _adjacents.end(); it++)
            result += "-" + it->first;
        result += "\n";
    }
    return result;
}

void Location::Print()
{
    cout <<_id <<". "<< _name << ": ";
    cout << _desc << endl;
}

string Location::getName()
{
    return _name;
}

string Location::getDescription()
{
    return _desc;
}

int Location::getID()
{
    return _id;
}

void Location::addItem(int id)
{
    _items.push_back(id);
}

void Location::removeItem(int id)
{
    for (int i = 0; i < _items.size(); i++)
    {
        if (_items[i] == id)
        _items.erase(_items.begin() + i);
    }
}

vector<int> Location::GetItems()
{
    return _items;
}

bool Location::isExistInDirections(string name)
{
    if (_direction.count(name) != 0)
        return true;
    map<string, string>::iterator it = _direction.begin();
    while (it != _direction.end())
    {
        if (it->second == name)
            return true;
        ++it;
    }
    return false;
}

void Location::onMessage(Message *msg)
{
    if (msg->GetType() == "release")
    {
        removeItem(atoi(msg->GetData().c_str()));
    }
    if (msg->GetType() == "take")
    {
        addItem(atoi(msg->GetData().c_str()));
    }
}
