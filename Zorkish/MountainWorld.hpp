//
//  MountainWorld.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef MountainWorld_hpp
#define MountainWorld_hpp

#include <iostream>
#include <fstream>
#include <vector>
#include "StringOperation.hpp"
#include "GameState.hpp"
#include "CommandManager.hpp"
#include "World.hpp"

using namespace std;

class MountainWorld : public GameState
{
public:
    World* _myWorld = World::NewWorld();
    
    void DisplayState();
    void HandleUserInput();
    static MountainWorld* reset();
    void Load(string fileName);
private:
    static MountainWorld* _instance;
    CommandManager* _cmdmanager = CommandManager::Instance(_myWorld);
protected:
    MountainWorld();
};

#endif /* MountainWorld_hpp */
