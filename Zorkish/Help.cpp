//
//  Help.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Help.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void Help::DisplayState()
{
    GameStateManager::ClearScreen();
    cout << "Zorkish :: Help" << endl;
    cout << "---------------------------------------" << endl;
    cout << "The following commands are supported: " << endl;
    cout << "\tquit, hiscore" << endl;
    cout << "Press ESC or Enter to return to the Main Menu" << endl;
}

void Help::HandleUserInput()
{
    char command;
    cin.ignore();
    cin.clear();
    command = cin.get();
    while (command != 27)
    {
        cout << "Press ESC or Enter to return to the Main Menu" << endl;
        command = cin.get();
    }
    GameStateManager::SwitchToMainMenu();
}

Help* Help::_instance = NULL;
Help* Help::Instance()
{
    if (_instance == NULL)
        _instance = new Help();
    return _instance;
}

Help::Help()
{
    
}
