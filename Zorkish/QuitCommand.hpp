//
//  QuitCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef QuitCommand_hpp
#define QuitCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class QuitCommand : public Command
{
public:
    QuitCommand(World* world);
    string Process(vector<string> args);
};

#endif /* QuitCommand_hpp */
