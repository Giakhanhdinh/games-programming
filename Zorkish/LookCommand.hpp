//
//  LookCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/6/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef LookCommand_hpp
#define LookCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class LookCommand : public Command
{
public:
    string Process(vector<string> args);
    LookCommand(World* world) : Command(world){}
};

#endif /* LookCommand_hpp */
