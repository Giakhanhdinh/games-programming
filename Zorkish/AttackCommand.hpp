//
//  AttackCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef AttackCommand_hpp
#define AttackCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class AttackCommand : public Command
{
public:
    AttackCommand(World* world) : Command(world){}
    string Process(vector<string> args);
};

#endif /* AttackCommand_hpp */
