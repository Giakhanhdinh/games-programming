//
//  CommandManager.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef CommandManager_hpp
#define CommandManager_hpp

#include <iostream>
#include "LookCommand.hpp"
#include "QuitCommand.hpp"
#include "HiscoreCommand.hpp"
#include "MoveCommand.hpp"
#include "InventoryCommand.hpp"
#include "PutCommand.hpp"
#include "AttackCommand.hpp"
#include "TakeCommand.hpp"
#include "FileOperation.hpp"
#include "StringOperation.hpp"
#include "ReverseCommand.hpp"
using namespace std;

class CommandManager : public Subscriber
{
private:
    CommandManager(World* world);
    static CommandManager* _instance;
    map<string, Command*> commandList;
    World* _world;
    Command* chosen;
public:
    static CommandManager* Instance(World* world);
    void Process(string input);
    World* GetWorld();
    void onMessage(Message* msg);
};

#endif /* CommandManager_hpp */
