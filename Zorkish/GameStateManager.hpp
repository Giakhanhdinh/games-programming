//
//  GameStateManager.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef GameStateManager_hpp
#define GameStateManager_hpp

#include <stdio.h>
#include "GameState.hpp"
#include "MainMenu.hpp"
#include "About.hpp"
#include "Help.hpp"
#include "SelectAdv.hpp"
#include "HallOfFame.hpp"
#include "NewHighscore.hpp"
#include "MountainWorld.hpp"
#include "WaterWorld.hpp"
#include "BoxWorld.hpp"
#include "Location.hpp"

class GameStateManager
{
private:
    static MainMenu* mainMenu;
    static About* about;
    static Help* help;
    static SelectAdv* selectAdv;
    static HallOfFame* hallOfFame;
    static NewHighscore* newHighScore;
    static GameState *currentState;
    static MountainWorld* moutainWorld;
    static WaterWorld* waterWorld;
    static BoxWorld* boxWorld;
    static bool isQuit;
public:
    GameStateManager();
    static void DisplayState();
    static void SwitchToMainMenu();
    static void SwitchToAbout();
    static void SwitchToHelp();
    static void SwitchToSelectAdv();
    static void SwitchToHallOfFame();
    static void SwitchToNewHighScore();
    static void HandleUserInput();
    static void SwitchToMountainWorld();
    static void SwitchToWaterWorld();
    static void SwitchToBoxWorld();
    static void QuitGame();
    static bool IsGameQuitRequested();
    static void ClearScreen();
};

#endif /* GameStateManager_hpp */
