//
//  CanContain.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef CanContain_hpp
#define CanContain_hpp

#include <iostream>
#include <vector>
using namespace std;

class CanContain
{
private:
    vector<int> _items;
public:
    void addItem(int id);
    void removeItem(int id);
    vector<int> GetItemsID();
};

#endif /* CanContain_hpp */
