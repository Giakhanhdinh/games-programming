//
//  StringOperation.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "StringOperation.hpp"
vector<string> SplitString(string original, string separator)
{
    vector<string> result;
    size_t pos = 0;
    while (pos != -1)
    {
        pos = original.find(separator);
        string buffer = original.substr(0, pos);
        original = original.substr(pos + 1);
        result.push_back(buffer);
    }
    return result;
}

vector<string> SplitStringOnce(string original, string separator)
{
    vector<string> result;
    size_t pos;
    pos = original.find(separator);
    string buffer = original.substr(0, pos);
    original = original.substr(pos + 1);
    result.push_back(buffer);
    result.push_back(original);
    return result;
}
