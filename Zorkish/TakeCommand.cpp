//
//  TakeCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/8/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "TakeCommand.hpp"
string TakeCommand::Process(vector<string> args)
{
    if (args.size() != 2 && args.size() != 4)
        return "Are you going to take something?";
    else if (args.size() == 2)
    {
        Item* foundItem = GetWorld()->FindItemWithNameInCurrentLoc(args[1]);
        if (foundItem == NULL || foundItem->GetComponentManager()->GetCanBePickedUp() == NULL)
            return "Cannot find this item or you tried to pick an un-pickable object";
        else
        {
            MessageQueue::AddMessage(new Message(GetWorld()->GetPlayerLocation(), GetWorld()->GetPlayer(), "take", to_string(foundItem->getID())));
            MessageQueue::AddMessage(new Message(GetWorld()->GetPlayer(), GetWorld()->GetPlayerLocation(), "release", to_string(foundItem->getID())));
            return "Take item successfully";
        }
    }
    else
    {
        if (args[2] != "from")
            return "Where do you want to take from";
        Item* foundItem = GetWorld()->FindItemWithNameInCurrentLoc(args[3]);
        if (foundItem == NULL || foundItem->GetComponentManager()->GetCanContain() == NULL)
            return "Cannot find this item or this item is not a container";
        Item* containedItem = GetWorld()->FindItemWithNameInAnotherItem(args[1], foundItem);
        if (containedItem == NULL)
            return "cannot find the item";
        MessageQueue::AddMessage(new Message(foundItem, GetWorld()->GetPlayer(), "take", to_string(containedItem->getID())));
        MessageQueue::AddMessage(new Message(GetWorld()->GetPlayer(), foundItem, "release", to_string(containedItem->getID())));
        return "Take command successfully";
    }
}
