//
//  Subscriber.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//
#ifndef Subscriber_hpp
#define Subscriber_hpp

#include <iostream>
using namespace std;

class Message;
class Subscriber
{
public:
    virtual void onMessage(Message* msg) = 0;
    
};

#endif /* Subscriber_hpp */
