//
//  PutCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/8/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "PutCommand.hpp"
string PutCommand::Process(vector<string> args)
{
    if (args.size() != 2 && args.size() != 4)
        return "Are you going to put something?";
    if (args.size() == 2)
    {
        Item* removedItem = GetWorld()->RemoveItemFromPlayer(args[1]);
        if (removedItem == NULL)
            return "Cannot find this item in your inventory";
        MessageQueue::AddMessage(new Message(GetWorld()->GetPlayerLocation(), GetWorld()->GetPlayer(), "release", to_string(removedItem->getID())));
        MessageQueue::AddMessage(new Message(GetWorld()->GetPlayer(), GetWorld()->GetPlayerLocation(), "take", to_string(removedItem->getID())));
        return "Put command successful";
    }
    if (args[2] != "in" && args[2] != "into")
        return "What do you want to put into?";
    Item* itemContained = GetWorld()->FindItemWithName(args[1]);
    Item* itemContain = GetWorld()->FindItemWithName(args[3]);
    if (itemContained == NULL || itemContain == NULL)
        return "Cannot find one or two items provided";
    if (itemContain->GetComponentManager()->GetCanContain() == NULL || itemContained ->GetComponentManager()->GetCanBePickedUp() == NULL)
        return "One or two of the items are now allowed to be picked or contain other items";
    Item* takenOut = GetWorld()->RemoveItemFromPlayer(args[1]);
    if (takenOut == NULL)
        return "Item to take out does not belong to you";
    Item* itemContainerInPlayer = GetWorld()->FindItemWithNameInPlayer(args[3]);
    Item* itemContainerInLoc = GetWorld()->FindItemWithNameInCurrentLoc(args[3]);
    if (itemContainerInPlayer != NULL)
    {
        MessageQueue::AddMessage(new Message(itemContainerInPlayer, GetWorld()->GetPlayer(), "release", to_string(takenOut->getID())));
        MessageQueue::AddMessage(new Message(GetWorld()->GetPlayer(), itemContainerInPlayer, "take", to_string(takenOut->getID())));
        return "Put command successful";
    }
    else if (itemContainerInLoc != NULL)
    {
        MessageQueue::AddMessage(new Message(itemContainerInLoc, GetWorld()->GetPlayer(), "release", to_string(takenOut->getID())));
        MessageQueue::AddMessage(new Message(GetWorld()->GetPlayer(), itemContainerInLoc, "take", to_string(takenOut->getID())));

        return "Put command successful";
    }
    return "Where do you want to put your item at?";
}
