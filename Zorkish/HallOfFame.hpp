//
//  HallOfFame.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//
#ifndef HallOfFame_hpp
#define HallOfFame_hpp

#include <stdio.h>
#include "GameState.hpp"
#include "FileOperation.hpp"
#include "StringOperation.hpp"

class HallOfFame : public GameState
{
public:
    void DisplayState();
    void HandleUserInput();
    static HallOfFame* Instance();
private:
    static HallOfFame* _instance;
protected:
    HallOfFame();
};

#endif /* HallOfFame_hpp */
