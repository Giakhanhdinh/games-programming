//
//  Command.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/13/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Command_hpp
#define Command_hpp

#include <iostream>
#include "World.hpp"
#include "StringOperation.hpp"
#include "Blackboard.hpp"
#include <vector>
using namespace std;

//class LookCommand
//{
//public:
//    void Process(World* world, string command);
//};

class Command
{
private:
    World* _world;
public:
    Command(World* world);
    virtual string Process(vector<string> args) = 0;
    World* GetWorld();
};
#endif /* Command_hpp */
