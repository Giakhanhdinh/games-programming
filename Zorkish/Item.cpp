//
//  Item.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Item.hpp"
Item::Item(string fromFile)
{
    _componentmanager = new ComponentManager();
    vector<string> attributes = SplitString(fromFile, ":");
    _id = stoi(attributes[0]);
    _name = attributes[1];
    _desc = attributes[2];
    if (attributes[3] != "")
    {
        vector<string> components = SplitString(attributes[3], ",");
        for (int i = 0; i < components.size(); i++)
            _componentmanager->RegisterComponent(components[i]);
    }
    if (attributes[4] != "")
    {
        vector<string> itemsinsideID = SplitString(attributes[4], ",");
        if (_componentmanager->GetCanContain() != NULL)
        {
            for (int i = 0; i < itemsinsideID.size(); i++)
                _componentmanager->GetCanContain()->addItem(stoi(itemsinsideID[i]));
        }
    }
}

ComponentManager* Item::GetComponentManager()
{
    return _componentmanager;
}

int Item::getID()
{
    return _id;
}

string Item::getName()
{
    return _name;
}

string Item::getDesc()
{
    return _desc;
}

void Item::onMessage(Message* message)
{
    string type = message->GetType();
    string data = message->GetData();
    if (type == "damage")
    {
        if (_componentmanager->GetHealth() != NULL)
        {
            int damage = stoi(data);
            _componentmanager->GetHealth()->Decrese(damage);
        }
    }
    if (type == "release")
    {
        _componentmanager->GetCanContain()->removeItem(atoi(message->GetData().c_str()));
    }
    if (type == "take")
    {
        _componentmanager->GetCanContain()->addItem(atoi(message->GetData().c_str()));
    }
}

