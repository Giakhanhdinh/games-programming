//
//  FileOperation.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/31/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "FileOperation.hpp"

vector<string> LoadFile(string fileName)
{
    vector<string> result;
    string line;
    ifstream myFile;
    myFile.open(fileName);
    
    while (getline(myFile, line))
    {
        result.push_back(line);
    }
    myFile.close();
    return result;
}
