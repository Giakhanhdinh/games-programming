//
//  CommandManager.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "CommandManager.hpp"
CommandManager::CommandManager(World* world)
{
    _world = world;
    commandList["LookCommand"] = new LookCommand(_world);
    commandList["QuitCommand"] = new QuitCommand(_world);
    commandList["HiscoreCommand"] = new HiscoreCommand(_world);
    commandList["MoveCommand"] = new MoveCommand(_world);
    commandList["TakeCommand"] = new TakeCommand(_world);
    commandList["PutCommand"] = new PutCommand(_world);
    commandList["AttackCommand"] = new AttackCommand(_world);
    commandList["InventoryCommand"] = new InventoryCommand(_world);
    commandList["ReverseCommand"] = new ReverseCommand(_world);
    vector<string> commandTypes = LoadFile("command.txt");
    for (int i = 0; i < commandTypes.size(); i++)
    {
        vector<string> typevsaliases = SplitStringOnce(commandTypes[i], ":");
        vector<string> aliases = SplitString(typevsaliases[1], ",");
        for (int j = 0; j < aliases.size(); j++)
            commandList[aliases[j]] = commandList[typevsaliases[0]];
    }
}

World* CommandManager::GetWorld()
{
    return _world;
}

CommandManager* CommandManager::Instance(World* world)
{
    _instance = new CommandManager(world);
    return _instance;
}

void CommandManager::Process(string input)
{
    vector<string> args = SplitString(input, " ");
    if (input == "")
    {
        Message* newmsg = new Message(this, Blackboard::GetInstance(), "write", "You put an empty command");
        MessageQueue::AddMessage(newmsg);
    }
    else if (commandList.count(args[0]) == 0)
    {
        Message* newmsg = new Message(this, Blackboard::GetInstance(), "write", "You put a illegal command");
        MessageQueue::AddMessage(newmsg);
    }
    else
    {
        chosen = commandList[args[0]];
        Message* newmsg = new Message(this, Blackboard::GetInstance(), "write", chosen->Process(args));
        MessageQueue::AddMessage(newmsg);
    }
}

CommandManager* CommandManager::_instance = NULL;
void CommandManager::onMessage(Message *msg)
{
    
}
