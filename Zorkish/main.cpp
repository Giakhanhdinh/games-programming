//
//  main.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 8/30/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include <iostream>
#include "GameStateManager.hpp"

int main(int argc, const char * argv[])
{
    while (!GameStateManager::IsGameQuitRequested())
    {
        GameStateManager::DisplayState();
        GameStateManager::HandleUserInput();
    }
    return 0;
}
