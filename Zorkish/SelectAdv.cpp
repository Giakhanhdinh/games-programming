//
//  SelectAdv.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "SelectAdv.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void SelectAdv::DisplayState()
{
    GameStateManager::ClearScreen();
    vector<string> toDisplay = LoadFile("selectadv.txt");
    for (int i = 0; i < toDisplay.size(); i++)
    {
        if (i > 2 && i < 6)
            cout << "\t" << toDisplay[i] << endl;
        else
            cout << toDisplay[i] << endl;
    }
}

void SelectAdv::HandleUserInput()
{
    char command;
    cin.ignore();
    cin.clear();
    command = cin.get();
    while (command != '1' && command != '2' && command != '3' && command != 27)
    {
        cout << endl << "Select 1 - 3: >";
        command = cin.get();
    }
    switch (command)
    {
        case '1':
            GameStateManager::SwitchToMountainWorld();
            break;
        case '2':
            GameStateManager::SwitchToWaterWorld();
            break;
        case '3':
            GameStateManager::SwitchToBoxWorld();
            break;
        case 27:
            GameStateManager::SwitchToMainMenu();
            break;
    }
}

SelectAdv* SelectAdv::_instance = NULL;
SelectAdv* SelectAdv::Instance()
{
    if (_instance == NULL)
        _instance = new SelectAdv();
    return _instance;
}

SelectAdv::SelectAdv()
{
    
}
