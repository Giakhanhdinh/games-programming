//
//  ComponentManager.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/5/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "ComponentManager.hpp"
Health* ComponentManager::GetHealth()
{
    return _health;
}

Damage* ComponentManager::GetDamage()
{
    return _damage;
}

CanBePickedUp* ComponentManager::GetCanBePickedUp()
{
    return _canbepickedup;
}

CanContain* ComponentManager::GetCanContain()
{
    return _cancontain;
}

void ComponentManager::InitiateHealth(int count)
{
    _health = new Health(count);
}

void ComponentManager::InitiateDamage(int count)
{
    _damage = new Damage(count);
}

void ComponentManager::InitiateCanContain()
{
    _cancontain = new CanContain();
}

void ComponentManager::InitiateCanBePickedUp()
{
    _canbepickedup = new CanBePickedUp();
}

void ComponentManager::RegisterComponent(string component)
{
    if (component == "health")
        InitiateHealth(90);
    if (component == "damage")
        InitiateDamage(50);
    if (component == "canbepickedup")
        InitiateCanBePickedUp();
    if (component == "cancontain")
        InitiateCanContain();
}
