//
//  MoveCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "MoveCommand.hpp"
string MoveCommand::Process(vector<string> args)
{
    if (args.size() != 2)
        return "Where do you want to move?";
    if (!GetWorld()->GetLocationAtId(GetWorld()->GetPlayer()->getRoomID())->isExistInDirections(args[1]))
        return "Illigal direction";
    int movedroomID = GetWorld()->MovePlayerTo(args[1]);
    if (movedroomID == -1)
        return "There is no exits to that direction";
    else
    {
        Message* newmsg = new Message(GetWorld()->GetPlayerLocation(), GetWorld()->GetPlayer(), "move", to_string(movedroomID));
        MessageQueue::AddMessage(newmsg);
    }
    return "You are at " + GetWorld()->GetPlayerLocation()->getName() + "\n" + GetWorld()->GetPlayerLocation()->getDescription();
}
