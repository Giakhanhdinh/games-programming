//
//  ReverseCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 11/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "ReverseCommand.hpp"

string ReverseCommand::Process(vector<string> args)
{
    if (args.size() != 1)
        return "Do you want to reverse?";
    Message* lastMess = MessageQueue::PopHistory();
    while (lastMess->GetType() == "write")
    {
        lastMess = MessageQueue::PopHistory();
    }
    if (lastMess->GetType() == "move")
    {
        Location* lastLoc = (Location*) lastMess->GetSender();
        MessageQueue::ReverseMessage(new Message(GetWorld()->GetPlayerLocation(), GetWorld()->GetPlayer(), oppositeType["move"], to_string(lastLoc->getID())));
    }
    else
    {
        Message* lastMess2 = MessageQueue::PopHistory();
        MessageQueue::ReverseMessage(new Message(lastMess->GetSender(), lastMess->GetRecipient(), oppositeType[lastMess->GetType()], lastMess->GetData()));
        MessageQueue::ReverseMessage(new Message(lastMess2->GetSender(), lastMess2->GetRecipient(), oppositeType[lastMess2->GetType()], lastMess->GetData()));
    }
        
    return "Reverse success";
}


