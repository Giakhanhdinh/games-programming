//
//  PutCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/8/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef PutCommand_hpp
#define PutCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class PutCommand : public Command
{
public:
    PutCommand(World* world) : Command(world){}
    string Process(vector<string> args);
};

#endif /* PutCommand_hpp */
