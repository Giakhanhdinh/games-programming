//
//  MessageQueue.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/10/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef MessageQueue_hpp
#define MessageQueue_hpp

#include <iostream>
#include <queue>
#include <stack>
#include <map>
using namespace std;
#include "Message.hpp"

class MessageQueue
{
private:
    MessageQueue();
    static std::queue<Message*> messages;
    static stack<Message*> history;
public:
    static void AddMessage(Message* newmsg);
    static void ReverseMessage(Message* newmsg);
    static Message* PopHistory();
    static void Dispatch();
};

#endif /* MessageQueue_hpp */
