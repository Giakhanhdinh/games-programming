//
//  GameState.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 8/30/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef GameState_hpp
#define GameState_hpp

#include <iostream>
using namespace std;

class GameState
{
public:
    virtual void DisplayState() = 0;
    virtual void HandleUserInput() = 0;
};

#endif /* GameState_hpp */
