//
//  Player.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <iostream>
#include <string>
#include "StringOperation.hpp"
#include "MessageQueue.hpp"
using namespace std;
class Player: public Subscriber
{
private:
    int _roomID;
    int _gold;
    vector<int> _items;
    static Player* _instance;
    Player(string fromFile);
public:
    static Player* NewPlayer(string fromFile);
    int getRoomID();
    void setRoomID(int newID);
    void AddItem(int id);
    void RemoveItem(int id);
    vector<int> GetItemsID();
    void onMessage(Message* msg);
};

#endif /* Player_hpp */
