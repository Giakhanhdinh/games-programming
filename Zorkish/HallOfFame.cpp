//
//  HallOfFame.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "HallOfFame.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void HallOfFame::DisplayState()
{
    GameStateManager::ClearScreen();
    vector<string> toDisplay = LoadFile("halloffame.txt");
    for (int i = 0; i < toDisplay.size(); i++)
    {
        vector<string> smallerDisplay = SplitString(toDisplay[i], ",");
        for (int j = 0; j < smallerDisplay.size(); j++)
        {
            if (j == 0)
                cout << smallerDisplay[j];
            else if (j == 1)
                cout << ". " << smallerDisplay[j];
            else
                cout << ", " << smallerDisplay[j];
        }
        cout << endl;
    }
}

void HallOfFame::HandleUserInput()
{
    char command;
    cin.ignore();
    cin.clear();
    command = cin.get();
    while (command != 27)
    {
        cout << "Press ESC or Enter to return to the Main Menu" << endl;
        command = cin.get();
    }
    GameStateManager::SwitchToMainMenu();
}

HallOfFame* HallOfFame::_instance = NULL;
HallOfFame* HallOfFame::Instance()
{
    if (_instance == NULL)
        _instance = new HallOfFame();
    return _instance;
}

HallOfFame::HallOfFame()
{
    
}
