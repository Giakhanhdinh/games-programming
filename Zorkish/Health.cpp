//
//  Health.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Health.hpp"
Health::Health(int count)
{
    _count = count;
}

int Health::Get()
{
    return _count;
}

void Health::Set(int count)
{
    _count = count;
}

void Health::Decrese(int decrement)
{
    _count -= decrement;
}
