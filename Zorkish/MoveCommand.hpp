//
//  MoveCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef MoveCommand_hpp
#define MoveCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class MoveCommand : public Command
{
public:
    MoveCommand(World* world) : Command(world){}
    string Process(vector<string> args);
};

#endif /* MoveCommand_hpp */
