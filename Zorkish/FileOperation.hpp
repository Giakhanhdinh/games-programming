//
//  FileOperation.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/31/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef FileOperation_hpp
#define FileOperation_hpp

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
using namespace std;

vector<string> LoadFile(string fileName);

#endif /* FileOperation_hpp */
