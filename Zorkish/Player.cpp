//
//  Player.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Player.hpp"
#include "Location.hpp"
Player* Player::_instance = NULL;
Player* Player::NewPlayer(string fromFile)
{
    _instance = new Player(fromFile);
    return _instance;
}

Player::Player(string fromFile)
{
    vector<string> attributes = SplitString(fromFile, ":");
    _roomID = stoi(attributes[0]);
}

int Player::getRoomID()
{
    return _roomID;
}

void Player::setRoomID(int newID)
{
    _roomID = newID;
}

void Player::AddItem(int id)
{
    _items.push_back(id);
}

void Player::RemoveItem(int id)
{
    for (int i = 0; i < _items.size(); i++)
    {
        if (_items[i] == id)
        _items.erase(_items.begin() + i);
    }
}

vector<int> Player::GetItemsID()
{
    return _items;
}

void Player::onMessage(Message *msg)
{
    if (msg->GetType() == "move")
    {
        string data = msg->GetData();
        _roomID = atoi(data.c_str());
    }
    if (msg->GetType() == "take")
    {
        AddItem(atoi(msg->GetData().c_str()));
    }
    
    if (msg->GetType() == "release")
    {
        RemoveItem(atoi(msg->GetData().c_str()));
    }
}

