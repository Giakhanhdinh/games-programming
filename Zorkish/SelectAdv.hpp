//
//  SelectAdv.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//
#ifndef SelectAdv_hpp
#define SelectAdv_hpp

#include <stdio.h>
#include "GameState.hpp"
#include "FileOperation.hpp"

class SelectAdv : public GameState
{
public:
    void DisplayState();
    void HandleUserInput();
    static SelectAdv* Instance();
private:
    static SelectAdv* _instance;
protected:
    SelectAdv();
};

#endif /* SelectAdv_hpp */
