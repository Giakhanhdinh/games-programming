//
//  Item.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Item_hpp
#define Item_hpp

#include <iostream>
#include "StringOperation.hpp"
#include "ComponentManager.hpp"
#include "MessageQueue.hpp"
#include <vector>
#include <cstdlib>
using namespace std;

class Item : public Subscriber
{
public:
    Item(string fromFile);
    int getID();
    string getName();
    string getDesc();
    ComponentManager* GetComponentManager();
    void onMessage(Message* message);
private:
    int _id;
    string _name;
    string _desc;
    ComponentManager* _componentmanager = new ComponentManager();
};

#endif /* Item_hpp */
