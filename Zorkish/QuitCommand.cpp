//
//  QuitCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "QuitCommand.hpp"
#include "GameStateManager.hpp"
QuitCommand::QuitCommand(World* world) : Command::Command(world)
{
}

string QuitCommand::Process(vector<string> args)
{
    GameStateManager::SwitchToMainMenu();
    return "";
}

