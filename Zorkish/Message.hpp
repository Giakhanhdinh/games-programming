//
//  Message.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Message_hpp
#define Message_hpp

#include <iostream>
using namespace std;
#include "Subscriber.hpp"

class Message
{
private:
    Subscriber* _from;
    Subscriber* _to;
    string _type;
    string _data;
public:
    Message(Subscriber* from, Subscriber* to, string type, string data);
    Subscriber* GetSender();
    Subscriber* GetRecipient();
    string GetType();
    string GetData();
};
#endif /* Message_hpp */
