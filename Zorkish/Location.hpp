//
//  Location.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Location_hpp
#define Location_hpp

#include <iostream>
#include <map>
#include <string>
#include "StringOperation.hpp"
#include "MessageQueue.hpp"
using namespace std;

class Location : public Subscriber
{
private:
    map<string, int> _adjacents;
    map<string, string> _direction;
    string _name;
    int _id;
    string _desc;
    vector<int> _items;
    
    void InitDirection();
    void CreateLocation(string fromFile);
public:
    string Direction(string key);
    Location(string fromFile);
    int GetAdjacent(string direction);
    string AdjacentDirections();
    void Print();
    string getName();
    string getDescription();
    int getID();
    vector<int> GetItems();
    void addItem(int id);
    void removeItem(int id);
    bool isExistInDirections(string name);
    void onMessage(Message *msg);
};

#endif /* Location_hpp */
