//
//  Health.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Health_hpp
#define Health_hpp

#include <iostream>
using namespace std;

class Health
{
private:
    int _count;
public:
    Health(int count);
    int Get();
    void Set(int count);
    void Decrese(int decrement);
};

#endif /* Health_hpp */
