S:3:Test world one. Basic locations and paths...
L:0: a field : standing at home sweet home. :u=1,s=3:0,5
L:1: a tree : up a tree for a nice view. Quite a nice view really. :d=0,w=4:1
L:3: a ponds edge : next to a medium sized pond. :n=0,e=5,w=2:3
L:5: a wall : standing next to a tall brick wall. :w=3
L:2: a castle : Great to settle and hide from monsters. :e=3,u=4
L:4: an observatory : Ideal place to take a look an entire view of the world. :e=1,d=2


I:0:rock: a small plain rock plus added dust:canbepickedup,damage
I:1:nest: a small birds nest made of twigs and mud:cancontain,canbepickedup:2
I:2:eggs: three small yellow eggs with brown dots:canbepickedup
I:3:pond: a medium sized pond:cancontain:4
I:4:water: pond water that is slightly green in colour:canbepickedup
I:5:monster: a monster that is approaching you:health
