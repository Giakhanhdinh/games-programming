//
//  BoxWorld.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef BoxWorld_hpp
#define BoxWorld_hpp

#include <stdio.h>
#include "GameState.hpp"
using namespace std;

class BoxWorld : public GameState
{
public:
    void DisplayState();
    void HandleUserInput();
    static BoxWorld* reset();
private:
    static BoxWorld* _instance;
protected:
    BoxWorld();
};

#endif /* BoxWorld_hpp */
