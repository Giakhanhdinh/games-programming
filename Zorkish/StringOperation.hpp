//
//  StringOperation.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef StringOperation_hpp
#define StringOperation_hpp

#include <iostream>
#include <string>
#include <vector>
using namespace std;

vector<string> SplitString(string original, string separator);
vector<string> SplitStringOnce(string original, string separator);




#endif /* StringOperation_hpp */
