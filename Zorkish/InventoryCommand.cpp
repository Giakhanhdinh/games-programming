//
//  InventoryCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/8/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "InventoryCommand.hpp"
string InventoryCommand::Process(vector<string> args)
{
    string result = "";
    if (args.size() != 1)
        return "Inventory command only has 1 word";
    result += "You have: \n";
    for (int i = 0; i < GetWorld()->GetPlayer()->GetItemsID().size(); i++)
        result += "- " + GetWorld()->GetItemAtId(GetWorld()->GetPlayer()->GetItemsID()[i])->getName();
    return result;
}
