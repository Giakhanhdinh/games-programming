//
//  MainMenu.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 8/31/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef MainMenu_hpp
#define MainMenu_hpp

#include "GameState.hpp"

class MainMenu : public GameState
{
public:
    void DisplayState();
    void HandleUserInput();
    static MainMenu* Instance();
private:
    static MainMenu* _instance;
protected:
    MainMenu();
};

#endif /* MainMenu_hpp */
