//
//  BoxWorld.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "BoxWorld.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void BoxWorld::DisplayState()
{
    GameStateManager::ClearScreen();
    cout << "Zorkish :: Box World" << endl;
    cout << "-------------------------------------------------" << endl;
    cout << "Input your command" << endl;
}

void BoxWorld::HandleUserInput()
{
    string command;
    cin.ignore();
    cin.clear();
    getline(cin, command);
    while (command != "hiscore" && command != "quit")
    {
        cout << "Input your command" << endl;
        getline(cin, command);
    }
    if (command == "hiscore")
        GameStateManager::SwitchToHallOfFame();
    if (command == "quit")
        GameStateManager::SwitchToMainMenu();
}

BoxWorld* BoxWorld::_instance = NULL;
BoxWorld* BoxWorld::reset()
{
    _instance = new BoxWorld();
    return _instance;
}

BoxWorld::BoxWorld()
{
    
}
