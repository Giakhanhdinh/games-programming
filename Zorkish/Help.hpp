//
//  Help.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//
#ifndef Help_hpp
#define Help_hpp

#include <stdio.h>
#include "GameState.hpp"

class Help : public GameState
{
public:
    void DisplayState();
    void HandleUserInput();
    static Help* Instance();
private:
    static Help* _instance;
protected:
    Help();
};

#endif /* Help_hpp */


