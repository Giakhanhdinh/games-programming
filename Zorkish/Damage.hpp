//
//  Damage.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Damage_hpp
#define Damage_hpp

#include <iostream>
using namespace std;

class Damage
{
private:
    int _count;
public:
    Damage(int count);
    int Get();
};

#endif /* Damage_hpp */
