//
//  NewHighscore.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//
#ifndef NewHighscore_hpp
#define NewHighscore_hpp

#include <stdio.h>
#include "GameState.hpp"


class NewHighscore : public GameState
{
public:
    void DisplayState();
    void HandleUserInput();
    static NewHighscore* Instance();
private:
    static NewHighscore* _instance;
protected:
    NewHighscore();
};

#endif /* NewHighscore_hpp */
