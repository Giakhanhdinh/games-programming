//
//  HiscoreCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "HiscoreCommand.hpp"
#include "GameStateManager.hpp"
HiscoreCommand::HiscoreCommand(World* world) : Command::Command(world)
{
}

string HiscoreCommand::Process(vector<string> args)
{
    if (args.size() != 1)
        return "Which hiscore do you want to look at?";
    GameStateManager::SwitchToHallOfFame();
    return "";
}
