//
//  World.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/12/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "World.hpp"
World* World::_instance = NULL;
World* World::NewWorld()
{
    _instance = new World();
    return _instance;
}

World::World()
{
    
}

void World::AddPlayer(string fromFile)
{
    _player = Player::NewPlayer(fromFile);
}

void World::AddLocation(string fromFile)
{
    Location* newLoc = new Location(fromFile);
    _rooms[newLoc->getID()] = newLoc;
}

int World::MovePlayerTo(string dir)
{
    int destinationID = _rooms[_player->getRoomID()]->GetAdjacent(dir);
    return destinationID;
}

string World::PlayerLocation()
{
    Location* currentLoc = _rooms[_player->getRoomID()];
    string result = "You are in " + currentLoc->getName() + "\n" + currentLoc->getDescription() + "\n";
    return result;
}

Location* World::GetPlayerLocation()
{
    return _rooms[_player->getRoomID()];
}

Player* World::GetPlayer()
{
    return _player;
}

Location* World::GetLocationAtId(int id)
{
    return _rooms[id];
}

Item* World::GetItemAtId(int id)
{
    return _items[id];
}

void World::AddItem(string fromFile)
{
    Item* newitem = new Item(fromFile);
    _items[newitem->getID()] = newitem;
}

Item* World::FindItemWithName(string name)
{
    for (int i = 0; i < _items.size(); i++)
        if (_items[i]->getName() == name)
            return _items[i];
    return NULL;
}

Item* World::FindItemWithNameInCurrentLoc(string name)
{
    Location* currentLoc = GetLocationAtId(_player->getRoomID());
    for (int i = 0; i < currentLoc->GetItems().size(); i++)
    {
        if (GetItemAtId(currentLoc->GetItems()[i])->getName() == name)
            return GetItemAtId(currentLoc->GetItems()[i]);
    }
    return NULL;
}

Item* World::FindItemWithNameInPlayer(string name)
{
    for (int i = 0; i < _player->GetItemsID().size(); i++)
    {
        if (GetItemAtId(_player->GetItemsID()[i])->getName() == name)
            return GetItemAtId(_player->GetItemsID()[i]);
    }
    return NULL;
}

string World::ItemDesc(int itemId)
{
    Item* item = _items[itemId];
    return item->getDesc() + "\n";
}

string World::ItemsOntheFloor()
{
    string result = "";
    Location* currentLoc = _rooms[_player->getRoomID()];
    if (currentLoc->GetItems().size() == 0)
        return result;
    else
    {
        result += "There are items: ";
        for (int i = 0; i < currentLoc->GetItems().size(); i++)
            result += "-" + _items[currentLoc->GetItems()[i]]->getName();
    }
    result += "\n";
    return result;
}

Item* World::FindItemWithNameInAnotherItem(string name, Item* item)
{
    vector<int> items = item->GetComponentManager()->GetCanContain()->GetItemsID();
    for (int i = 0; i < items.size(); i++)
    {
        if (GetItemAtId(items[i])->getName() == name)
            return GetItemAtId(items[i]);
    }
    return NULL;
}

vector<Item*> World::GetItemsInsideAnItem(Item* item)
{
    vector<Item*> result;
    vector<int> itemsinside = item->GetComponentManager()->GetCanContain()->GetItemsID();
    for (int i = 0; i < itemsinside.size(); i++)
        result.push_back(_items[itemsinside[i]]);
    return result;
}

void World::AddItemToPlayer(int id)
{
    _player->AddItem(id);
}

Item* World::RemoveItemFromPlayer(string name)
{
    Item* itemToRemove = FindItemWithName(name);
    if (itemToRemove != NULL)
    {
        return itemToRemove;
    }
    return NULL;
}

Item* World::RemoveItemFromCurrentLoc(string name)
{
    Item* foundItem = FindItemWithNameInCurrentLoc(name);
    Location* currentLoc = GetPlayerLocation();
    if (foundItem != NULL)
    {
        currentLoc->removeItem(foundItem->getID());
        return foundItem;
    }
    return NULL;
}

void World::AddItemToCurrentLoc(int id)
{
    GetPlayerLocation()->addItem(id);
}

void World::RemoveItemFromContainer(Item* container, Item* containedItem)
{
    container->GetComponentManager()->GetCanContain()->removeItem(containedItem->getID());
}

void World::AddItemToContainer(Item* container, Item* containedItem)
{
    container->GetComponentManager()->GetCanContain()->addItem(containedItem->getID());
}

