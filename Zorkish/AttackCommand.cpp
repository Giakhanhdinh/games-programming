//
//  AttackCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "AttackCommand.hpp"
string AttackCommand::Process(vector<string> args)
{
    if (args.size() != 4)
        return "could you execute the attack again?";
    if (args[2] != "with")
        return "what do you want to attack with";
    Item* weapon = GetWorld()->FindItemWithNameInPlayer(args[3]);
    Item* target = GetWorld()->FindItemWithNameInCurrentLoc(args[1]);
    if (weapon == NULL || target == NULL || weapon->GetComponentManager()->GetDamage() == NULL)
        return "1 or 2 items are not available";
    Message* newmsg = new Message(weapon, target, "damage", to_string(weapon->GetComponentManager()->GetDamage()->Get()));
    MessageQueue::AddMessage(newmsg);
    return "Attack successful. Current health of the target is: " + to_string(target->GetComponentManager()->GetHealth()->Get());
}
