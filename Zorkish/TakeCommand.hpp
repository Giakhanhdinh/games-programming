//
//  TakeCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/8/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef TakeCommand_hpp
#define TakeCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class TakeCommand : public Command
{
public:
    TakeCommand(World* world) : Command(world){}
    string Process(vector<string> args);
};

#endif /* TakeCommand_hpp */
