//
//  Blackboard.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/31/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef Blackboard_hpp
#define Blackboard_hpp

#include <iostream>
#include "MessageQueue.hpp"
using namespace std;

class Blackboard : public Subscriber
{
private:
    static Blackboard* board;
    Blackboard();
public:
    static Blackboard* GetInstance();
    void onMessage(Message* msg);
    
};

#endif /* Blackboard_hpp */
