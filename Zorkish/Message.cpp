//
//  Message.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/9/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "Message.hpp"
Message::Message(Subscriber* from, Subscriber* to, string type, string data)
{
    _from = from;
    _to = to;
    _type = type;
    _data = data;
}

Subscriber* Message::GetSender()
{
    return _from;
}

Subscriber* Message::GetRecipient()
{
    return _to;
}

string Message::GetType()
{
    return _type;
}

string Message::GetData()
{
    return _data;
}
