//
//  CanContain.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/3/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "CanContain.hpp"
void CanContain::addItem(int id)
{
    _items.push_back(id);
}

void CanContain::removeItem(int id)
{
    for (int i = 0; i < _items.size(); i++)
    {
        if (_items[i] == id)
            _items.erase(_items.begin() + i);
    }
}

vector<int> CanContain::GetItemsID()
{
    return _items;
}
