//
//  GameStateManager.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

MainMenu* GameStateManager::mainMenu = MainMenu::Instance();
About* GameStateManager::about = About::Instance();
Help* GameStateManager::help = Help::Instance();
SelectAdv* GameStateManager::selectAdv = SelectAdv::Instance();
HallOfFame* GameStateManager::hallOfFame = HallOfFame::Instance();
NewHighscore* GameStateManager::newHighScore = NewHighscore::Instance();
GameState* GameStateManager::currentState = mainMenu;
bool GameStateManager::isQuit = false;
MountainWorld* GameStateManager::moutainWorld = MountainWorld::reset();
WaterWorld* GameStateManager::waterWorld = WaterWorld::reset();
BoxWorld* GameStateManager::boxWorld = BoxWorld::reset();


void GameStateManager::DisplayState()
{
    currentState->DisplayState();
}

void GameStateManager::SwitchToMainMenu()
{
    currentState = mainMenu;
}

void GameStateManager::SwitchToAbout()
{
    currentState = about;
}

void GameStateManager::SwitchToHelp()
{
    currentState = help;
}

void GameStateManager::SwitchToSelectAdv()
{
    currentState = selectAdv;
}

void GameStateManager::SwitchToHallOfFame()
{
    currentState = hallOfFame;
}

void GameStateManager::SwitchToNewHighScore()
{
    currentState = newHighScore;
}

void GameStateManager::HandleUserInput()
{
    currentState->HandleUserInput();
}

void GameStateManager::QuitGame()
{
    isQuit = true;
}

bool GameStateManager::IsGameQuitRequested()
{
    return isQuit;
}

void GameStateManager::ClearScreen()
{
    for (int i = 0; i < 100; i++)
        cout << endl;
}

void GameStateManager::SwitchToBoxWorld()
{
    currentState = boxWorld;
}

void GameStateManager::SwitchToWaterWorld()
{
    currentState = waterWorld;
}

void GameStateManager::SwitchToMountainWorld()
{
    currentState = moutainWorld;
    moutainWorld->Load("testworld.txt");
}

