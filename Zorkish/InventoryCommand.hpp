//
//  InventoryCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/8/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef InventoryCommand_hpp
#define InventoryCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class InventoryCommand : public Command
{
public:
    InventoryCommand(World* world) : Command(world){}
    string Process(vector<string> args);
};

#endif /* InventoryCommand_hpp */
