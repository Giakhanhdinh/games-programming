//
//  NewHighscore.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 9/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "NewHighscore.hpp"
#include "GameStateManager.hpp"
#include <iostream>
using namespace std;

void NewHighscore::DisplayState()
{
    GameStateManager::ClearScreen();
    cout << "Zorkish :: New High Score" << endl;
    cout << "-----------------------------------------------" << endl;
    cout << "Congratulations!" << endl;
    cout << "You made it to the Zorkish Hall Of Fame" << endl;
    cout << endl;
    cout << "Adventure: " << endl;
    cout << "Score: " << endl;
    cout << "Moves: " << endl;
    cout << endl;
    cout << "Please type your name and press enter: >  ";
}

void NewHighscore::HandleUserInput()
{
    string command;
    cin.ignore();
    cin.clear();
    getline(cin, command);
    GameStateManager::SwitchToMainMenu();
}

NewHighscore* NewHighscore::_instance = NULL;
NewHighscore* NewHighscore::Instance()
{
    if (_instance == NULL)
        _instance = new NewHighscore();
    return _instance;
}

NewHighscore::NewHighscore()
{
    
}
