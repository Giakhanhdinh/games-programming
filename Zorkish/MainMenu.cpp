//
//  MainMenu.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 8/31/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "MainMenu.hpp"
#include "GameStateManager.hpp"
#include <iostream>

using namespace std;
void MainMenu::DisplayState()
{
    GameStateManager::ClearScreen();
    cout << "Zorkish :: Main Menu" << endl;
    cout << "--------------------------------------------" << endl;
    cout << endl;
    cout << "Welcome to Zorkish Adventure" << endl;
    cout << "1. Select Adventure and Play" << endl;
    cout << "2. Hall of Fame" << endl;
    cout << "3. Help" << endl;
    cout << "4. About" << endl;
    cout << "5. Quit" << endl;
    cout << "Select 1 - 5:>";
}

void MainMenu::HandleUserInput()
{
    char command;
    //cin.ignore();
    cin.clear();
    command = cin.get();
    while (command != '1' && command != '2' && command != '3' && command != '4' && command != '5')
    {
        cout << "Invalid command. Select 1 - 5: > ";
        command = cin.get();
    }
    switch (command)
    {
        case '1':
            GameStateManager::SwitchToSelectAdv();
            break;
        case '2':
            GameStateManager::SwitchToHallOfFame();
            break;
        case '3':
            GameStateManager::SwitchToHelp();
            break;
        case '4':
            GameStateManager::SwitchToAbout();
            break;
        case '5':
            GameStateManager::QuitGame();
            break;
        case 27:
            GameStateManager::SwitchToMainMenu();
            break;
        default:
            break;
    }
}

MainMenu* MainMenu::_instance = NULL;
MainMenu* MainMenu::Instance()
{
    if (_instance == NULL)
        _instance = new MainMenu;
    return _instance;
}

MainMenu::MainMenu()
{
    
}

