//
//  LookCommand.cpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/6/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "LookCommand.hpp"
string LookCommand::Process(vector<string> args)
{
    string result ="";
    if (args.size() == 1)
    {
        result += GetWorld()->PlayerLocation();
        result += GetWorld()->GetLocationAtId((GetWorld()->GetPlayer()->getRoomID()))->AdjacentDirections();
        result += GetWorld()->ItemsOntheFloor();
    }
    else if (args.size() == 3)
    {
        if (args[1] == "at")
        {
            Item* foundItem = GetWorld()->FindItemWithNameInCurrentLoc(args[2]);
            if (foundItem != NULL)
                result += foundItem->getDesc() + "\n";
            else
                result += "Can't find the item\n";
        }
        else if (args[1] == "in" || args[1] == "into")
        {
            Item* foundItem = GetWorld()->FindItemWithNameInCurrentLoc(args[2]);
            if (foundItem != NULL)
            {
                if (foundItem->GetComponentManager()->GetCanContain() == NULL)
                    result += "This item cannot contain other items\n";
                else
                {
                    vector<Item*> itemsinside = GetWorld()->GetItemsInsideAnItem(foundItem);
                    if (itemsinside.size() == 0)
                        result += "There is no item inside\n";
                    else
                    {
                        result += "There are/is ";
                        for (int i = 0; i < itemsinside.size(); i++)
                            result += "-"+itemsinside[i]->getName()+" ";
                        result += "\n";
                    }
                }
            }
        }
        else
        {
            result += "Where are you looking at?\n";
        }
    }
    
    else if (args.size() == 5)
    {
        if (args[1] != "at" || args[3] != "in")
            result += "I don't know how to look like that";
        else
        {
            Item* foundItem = GetWorld()->FindItemWithNameInCurrentLoc(args[4]);
            if (foundItem != NULL)
            {
                if (foundItem->GetComponentManager()->GetCanContain() == NULL)
                    result += "This item cannot contain other items\n";
                else
                {
                    Item* inside = GetWorld()->FindItemWithNameInAnotherItem(args[2], foundItem);
                    if (inside == NULL)
                        result += "There is no item with that name inside\n";
                    else
                        result += inside->getDesc() + "\n";
                }
            }
        }
    }
    else
    {
        result += "It's an illegal command";
    }
    return result;
}

