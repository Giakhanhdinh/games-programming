//
//  ReverseCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 11/1/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef ReverseCommand_hpp
#define ReverseCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class ReverseCommand : public Command
{
public:
    ReverseCommand(World* world) : Command(world)
    {
        oppositeType["release"] = "take";
        oppositeType["take"] = "release";
        oppositeType["damage"] = "heal";
        oppositeType["heal"] = "damage";
        oppositeType["move"] = "move";
    }
    string Process(vector<string> args);
private:
    map<string, string> oppositeType;
};

#endif /* ReverseCommand_hpp */
