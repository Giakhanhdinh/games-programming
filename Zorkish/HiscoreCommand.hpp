//
//  HiscoreCommand.hpp
//  Zorkish
//
//  Created by Khanh Dinh on 10/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef HiscoreCommand_hpp
#define HiscoreCommand_hpp

#include <iostream>
#include "Command.hpp"
using namespace std;

class HiscoreCommand : public Command
{
public:
    HiscoreCommand(World* world);
    string Process(vector<string> args);
};
#endif /* HiscoreCommand_hpp */
