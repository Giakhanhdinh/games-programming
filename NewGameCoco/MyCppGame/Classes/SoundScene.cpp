//
//  SoundScene.cpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/18/17.
//

#include "SoundScene.hpp"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"

USING_NS_CC;

cocos2d::Scene* SoundScene::createScene()
{
    auto scene = cocos2d::Scene::create();
    auto layer = SoundScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool SoundScene::init()
{
    if (!Layer::init())
        return false;
    
    auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
    audio->preloadBackgroundMusic("song.mp3");
    audio->preloadBackgroundMusic("song1.mp3");
    audio->preloadEffect("gun-shot-01.wav");
    audio->preloadEffect("gun-cocking-01.wav");
    audio->preloadEffect("shell-falling.wav");
    
    
    auto eventListener = cocos2d::EventListenerKeyboard::create();
    eventListener->onKeyPressed = [audio, this] (cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event* event)
    {
        switch (keyCode) {
            case cocos2d::EventKeyboard::KeyCode::KEY_SPACE:
                if (isMusicPlaying)
                    audio->pauseBackgroundMusic();
                else
                    audio->resumeBackgroundMusic();
                isMusicPlaying = !isMusicPlaying;
                break;
                
            case cocos2d::EventKeyboard::KeyCode::KEY_RIGHT_ARROW:
                audio->pauseBackgroundMusic();
                audio->playBackgroundMusic("song1.mp3", false);
                break;
                
            case cocos2d::EventKeyboard::KeyCode::KEY_LEFT_ARROW:
                audio->pauseBackgroundMusic();
                audio->playBackgroundMusic("song.mp3", false) ;
                break;
            case cocos2d::EventKeyboard::KeyCode::KEY_1:
                audio->playEffect("gun-cocking-01.wav");
                break;
            case cocos2d::EventKeyboard::KeyCode::KEY_2:
                audio->playEffect("gun-shot-01.wav");
                break;
            case cocos2d::EventKeyboard::KeyCode::KEY_3:
                audio->playEffect("shell-falling.wav");
                break;
            default:
                break;
        }
    };
    _eventDispatcher->addEventListenerWithFixedPriority(eventListener, 2);
    
    return true;
}
