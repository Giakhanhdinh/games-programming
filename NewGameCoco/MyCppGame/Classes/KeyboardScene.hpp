//
//  KeyboardScene.hpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/17/17.
//

#pragma once

#include "cocos2d.h"
#include "string.h"
#include <map>


class KeyboardScene : public cocos2d::Layer
{
public:
    
    static cocos2d::Scene* createScene();
    virtual bool init();
    
    bool isKeyPressed(cocos2d::EventKeyboard::KeyCode);
    double keyPressedDuration(cocos2d::EventKeyboard::KeyCode);
    
    CREATE_FUNC(KeyboardScene);
    
private:
    static std::map<cocos2d::EventKeyboard::KeyCode,
    std::chrono::high_resolution_clock::time_point> keys;
    cocos2d::Label * label;
    char str[2] = {'X','-'};
    char* print = (char*)malloc(10);
public:
    virtual void update(float delta) override;
};
