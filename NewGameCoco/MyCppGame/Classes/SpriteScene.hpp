//
//  SpriteScene.hpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/18/17.
//

#pragma once
#include "cocos2d.h"

class SpriteScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(SpriteScene);
};

