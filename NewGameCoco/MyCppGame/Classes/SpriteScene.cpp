//
//  SpriteScene.cpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/18/17.
//

#include "SpriteScene.hpp"
#include <cstdlib>
USING_NS_CC;

Scene* SpriteScene::createScene()
{
    auto scene = Scene::create();
    auto layer = SpriteScene::create();
    
    scene->addChild(layer);
    return scene;
}

bool SpriteScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    cocos2d::log("OOO");
    auto spritecache = SpriteFrameCache::getInstance();
    spritecache->addSpriteFramesWithFile("mySprites.plist");
    auto mysprite1 = Sprite::createWithSpriteFrameName("ship1.png");
    auto mysprite2 = Sprite::createWithSpriteFrameName("ship2.png");
    auto mysprite3 = Sprite::createWithSpriteFrameName("ship3.png");
    mysprite1->setVisible(false);
    mysprite2->setVisible(false);
    mysprite3->setVisible(false);
    
    auto sprite = Sprite::create("star.png");
    sprite->setAnchorPoint(Vec2(0.0,0.0));
    this->addChild(sprite,0);
    this->addChild(mysprite1, 1);
    this->addChild(mysprite2, 1);
    this->addChild(mysprite3, 1);
    
    auto eventListener = EventListenerKeyboard::create();
    eventListener->onKeyPressed = [=](EventKeyboard::KeyCode keyCode, Event* event)
    {
        if (keyCode == EventKeyboard::KeyCode::KEY_0)
        {
            if (sprite->isVisible())
                sprite->setVisible(false);
            else
                sprite->setVisible(true);
        }
        if (keyCode == EventKeyboard::KeyCode::KEY_1)
        {
            if (mysprite1->isVisible()) {
                mysprite1->setVisible(false);
            }
            else
            {
                mysprite1->setPosition(rand()%800+1, rand()%600 + 1);
                mysprite1->setVisible(true);
            }
        }
        if (keyCode == EventKeyboard::KeyCode::KEY_2)
        {
            if (mysprite2->isVisible()) {
                mysprite2->setVisible(false);
            }
            else
            {
                mysprite2->setPosition(rand()%800+1, rand()%600 + 1);
                mysprite2->setVisible(true);
            }
        }
        if (keyCode == EventKeyboard::KeyCode::KEY_3)
        {
            if (mysprite3->isVisible()) {
                mysprite3->setVisible(false);
            }
            else
            {
                mysprite3->setPosition(rand()%800+1, rand()%600 + 1);
                mysprite3->setVisible(true);
            }
        }

    };
    _eventDispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);
    
    return true;
}





