//
//  CollisionSceneCircle.hpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/25/17.
//

#pragma once
#include "cocos2d.h"
using namespace cocos2d;

class CollisionSceneCircle : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    void update(float dt);
    void drawScene(Color4F color);

    CREATE_FUNC(CollisionSceneCircle);
private:
    DrawNode* myCircleNode;
    float cirRadX1;
    float cirRadY1;
    float cirRadX2;
    float cirRadY2;
    Vec2 center;
    Vec2 center2;
    Color4F white;
    Color4F other;
};

