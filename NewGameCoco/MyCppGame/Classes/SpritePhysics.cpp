//
//  Sprite.cpp
//  MyCppGame
//
//  Created by Khanh Dinh on 11/1/17.
//

#include "SpritePhysics.hpp"
SpritePhysics::SpritePhysics(int id, int positionX, int positionY, Vec2 velocity, std::string fileName)
{
    _id = id;
    _positionX = positionX;
    _positionY = positionY;
    _velocity = velocity;
    _sprite = fileName;
}

int SpritePhysics::GetX()
{
    return _positionX;
}

int SpritePhysics::GetY()
{
    return _positionY;
}

Vec2 SpritePhysics::GetVelocity()
{
    return _velocity;
}

int SpritePhysics::GetId()
{
    return _id;
}

std::string SpritePhysics::GetSprite()
{
    return _sprite;
}

