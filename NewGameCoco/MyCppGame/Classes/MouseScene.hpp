//
//  MouseScene.hpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/17/17.
//

#pragma once
#include "cocos2d.h"
#include "base/CCEventMouse.h"

class MouseScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(MouseScene);
};

