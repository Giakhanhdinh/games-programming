//
//  CollisionSceneCircle.cpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/25/17.
//

#include "CollisionSceneCircle.hpp"
USING_NS_CC;

Scene* CollisionSceneCircle::createScene()
{
    auto scene = Scene::create();
    auto layer = CollisionSceneCircle::create();
    
    scene->addChild(layer);
    return scene;
}

bool CollisionSceneCircle::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    cirRadX1 = 30;
    cirRadX2 = 200;
    cirRadY1 = 30;
    cirRadY2 = 200;
    
    this->scheduleUpdate();
    myCircleNode = DrawNode::create();
    center = Vec2(cirRadX1, cirRadY1);
    center2 = Vec2(cirRadX2, cirRadY2);
    white = Color4F(1,1,1,1);
    other = Color4F(123,101,4,124);
    
    myCircleNode->drawDot(center, 30, white);
    myCircleNode->drawDot(center2, 30, white);
    this->addChild(myCircleNode);
    return true;
}

void CollisionSceneCircle::update(float dt)
{
    cirRadX1 = cirRadX1 + 1;
    cirRadY1 = cirRadY1 + 1;
    center = Vec2(cirRadX1, cirRadY1);
    if (center.distance(center2) > 60)
        drawScene(white);
    else
        drawScene(other);
}

void CollisionSceneCircle::drawScene(Color4F color)
{
    myCircleNode->clear();
    myCircleNode->drawDot(center, 30, color);
    myCircleNode->drawDot(center2, 30, color);
}
