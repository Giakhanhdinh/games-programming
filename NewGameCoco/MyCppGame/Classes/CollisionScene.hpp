//
//  CollisionScene.hpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/19/17.
//

#pragma once
#include "cocos2d.h"

class CollisionScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    
    virtual bool init();
    void DrawRect(cocos2d::Rect* rect, cocos2d::DrawNode* rectNode);
    void update(float dt);
    void setRectPos(cocos2d::Rect* mRect, cocos2d::DrawNode* rectNode, float newX, float newY, float width, float height);
    
    // implement the "static create()" method manually
    CREATE_FUNC(CollisionScene);
private:
    cocos2d::DrawNode* rectNode1;
    cocos2d::DrawNode* rectNode2;
    cocos2d::Rect* mRect1;
    cocos2d::Rect* mRect2;
    
};



