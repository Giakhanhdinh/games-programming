//
//  Sprite.hpp
//  MyCppGame
//
//  Created by Khanh Dinh on 11/1/17.
//

#ifndef Sprite_hpp
#define Sprite_hpp

#include "cocos2d.h"
using namespace cocos2d;

class SpritePhysics
{
private:
    int _id;
    std::string _sprite;
    int _positionX;
    int _positionY;
    Vec2 _velocity;
    PhysicsBody* _body;
public:
    SpritePhysics(int id, int positionX, int positionY, Vec2 velocity, std::string fileName);
    int GetX();
    int GetY();
    Vec2 GetVelocity();
    int GetId();
    std::string GetSprite();
    
};

#endif /* Sprite_hpp */
