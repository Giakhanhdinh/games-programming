//
//  CollisionScene.cpp
//  MyCppGame
//
//  Created by Khanh Dinh on 10/19/17.
//

#include "CollisionScene.hpp"
USING_NS_CC;

Scene* CollisionScene::createScene()
{
    auto scene = Scene::create();
    auto layer = CollisionScene::create();
    
    scene->addChild(layer);
    return scene;
}

bool CollisionScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    mRect1 = new Rect(0,0,50,50);
    mRect2 = new Rect(200,200,50,50);
    rectNode1 = DrawNode::create();
    rectNode2 = DrawNode::create();
    DrawRect(mRect1, rectNode1);
    DrawRect(mRect2, rectNode2);

    this->scheduleUpdate();
    this->addChild(rectNode1);
    this->addChild(rectNode2);
    return true;
}

void CollisionScene::DrawRect(Rect* mRect, DrawNode* rectNode)
{
    Vec2 rectangle[4];
    rectangle[0] = Vec2(mRect->getMinX(), mRect->getMinY());
    rectangle[1] = Vec2(mRect->getMaxX(), mRect->getMinY());
    rectangle[2] = Vec2(mRect->getMaxX(), mRect->getMaxY());
    rectangle[3] = Vec2(mRect->getMinX(), mRect->getMaxY());
    
    Color4F white(1, 1, 1, 1);
    rectNode->drawPolygon(rectangle, 4, white, 1, white);

}

void CollisionScene::update(float dt)
{
    setRectPos(mRect1,rectNode1, rectNode1->getPosition().x + 1, rectNode1->getPosition().y + 1, 50, 50);
    if (mRect1->intersectsRect(*mRect2))
        CCLOG("Collided");
    else
        CCLOG("Over");
}

void CollisionScene::setRectPos(Rect* mRect, DrawNode* rectNode, float newX, float newY, float width, float height)
{
    mRect->setRect(newX, newY, width, height);
    rectNode->setPosition(newX, newY);
}

    
    
