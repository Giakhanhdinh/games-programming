#include "PhysicsCollisionScene.hpp"

USING_NS_CC;

Scene* PhysicsCollisionScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setDebugDrawMask( PhysicsWorld::DEBUGDRAW_ALL );
    
    scene->getPhysicsWorld()->setGravity(Vec2(0,0));
    
    //scene->getPhysicsWorld()->setGravity( Vec2( 0, 0 ) );
    
    // 'layer' is an autorelease object
    auto layer = PhysicsCollisionScene::create();
    layer->SetPhysicsWorld( scene->getPhysicsWorld() );
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool PhysicsCollisionScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto edgeBody = PhysicsBody::createEdgeBox( visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 4 );
    
    auto edgeNode = Node::create();
    edgeNode ->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    edgeNode->setPhysicsBody( edgeBody );
    
    this->addChild( edgeNode );
    srand (time(NULL));
    AddSprite(new SpritePhysics(1, rand()% (int)visibleSize.width, rand()% static_cast<int>(visibleSize.height), Vec2(200, 150), "CloseNormal.png"));
    AddSprite(new SpritePhysics(2, rand()% (int)visibleSize.width, rand()% static_cast<int>(visibleSize.height), Vec2(200, 150), "CloseNormal.png"));
    
    auto keyboardListener = EventListenerKeyboard::create();
    auto contactListener = EventListenerPhysicsContact::create();
    
    contactListener->onContactBegin =CC_CALLBACK_1(PhysicsCollisionScene::onContactBegin, this);
    keyboardListener->onKeyPressed = [this] (EventKeyboard::KeyCode key, Event* event)
    {
        if (key == EventKeyboard::KeyCode::KEY_SPACE)
        {
            AddSprite(new SpritePhysics(3, rand() % 900, rand() % 600, Vec2(200, 150), "CloseNormal.png"));
        }
    };
    
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
    return true;
}

bool PhysicsCollisionScene::onContactBegin(PhysicsContact &contact)
{
    PhysicsBody *a = contact.getShapeA()->getBody();
    PhysicsBody *b = contact.getShapeB()->getBody();
    
    //check
    if (((1 == a->getCollisionBitmask()) && (2 == b->getCollisionBitmask())) || ((2 == a->getCollisionBitmask()) && (1 == b->getCollisionBitmask())))
        CCLOG("Collided");
    return true;
}


void PhysicsCollisionScene::AddSprite(SpritePhysics* newSprite)
{
    auto sprite = Sprite::create(newSprite->GetSprite());
    sprite->setPosition(Point(newSprite->GetX(), newSprite->GetY()));
    auto spriteBody = PhysicsBody::createBox(sprite->getContentSize(),PhysicsMaterial(0,1,0));
    spriteBody->setVelocity(newSprite->GetVelocity());
    spriteBody->setCollisionBitmask(newSprite->GetId());
    spriteBody->setContactTestBitmask(true);
    sprite->setPhysicsBody(spriteBody);
    
    this->addChild(sprite);
}



