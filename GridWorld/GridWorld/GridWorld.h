//
//  GridWorld.h
//  GridWorld
//
//  Created by Khanh Dinh on 9/4/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#ifndef GridWorld_h
#define GridWorld_h
#include <iostream>
using namespace std;
enum Cell
{
    Wall, Gold, Demise, Way, Player
};

struct Grid
{
    Cell Cells[8][9];
};

enum GameStatus
{
    Win, Lose, InProgress
};

struct Game
{
    Grid Grid;
    int PlayerLocationX;
    int PlayerLocationY;
    GameStatus Status;
};

char CellChar(Cell cell);
char ToLower(char &c);
void DrawCell(Cell cell);
void DrawGrid(Grid grid);
void SetUpGrid(Grid &grid);
void SetUpPlayer(Game &game);
void SetUpGame(Game &game);
void WriteInstruction(Game game);
void MoveThePlayer(Game &game, int toX, int toY);
void UpdateWinLose(Game &game, char command);
void UpdateMovement(Game &game, char command);
void UpdateGame(Game &game, char command);
void StartGame(Game &game);
#endif /* GridWorld_h */
