//
//  GridWorld.cpp
//  GridWorld
//
//  Created by Khanh Dinh on 9/4/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include <iostream>
#include "GridWorld.h"
#include <SDL2/SDL.h>
#include <thread>
using namespace std;

char command = '\0';
bool gameBlock = false;

char CellChar(Cell cell)
{
    switch (cell)
    {
        case Wall:
            return '*';
            break;
        case Gold:
            return 'G';
            break;
        case Demise:
            return 'D';
            break;
        case Player:
            return 'S';
            break;
        default:
            return ' ';
            break;
    }
}

char ToLower(char &c)
{
    if (c >= 65 && c <= 90)
        return c + 32;
    return c;
}

void DrawCell(Cell cell)
{
    cout << CellChar(cell);
}

void DrawGrid(Grid grid)
{
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            
            DrawCell(grid.Cells[j][i]);
        }
        cout << endl;
    }
}

void SetUpGrid(Grid &grid)
{
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (i == 0 || i == 7)
                grid.Cells[j][i] = Wall;
            else if ((j == 0 || j == 7) && i != 8)
                grid.Cells[j][i] = Wall;
            else if (i == 3 && (j == 1 || j == 2))
                grid.Cells[j][i] = Wall;
            else if (j == 4 && (i >= 1 && i <= 4))
                grid.Cells[j][i] = Wall;
            else if (i == 5 && (j >= 2 && j <= 5))
                grid.Cells[j][i] = Wall;
            else if (i == 1 && j == 1)
                grid.Cells[j][i] = Gold;
            else if ((j == 3 && i == 1) || (j == 5 && i == 1) || (j == 6 && i == 3))
                grid.Cells[j][i] =  Demise;
            else if (j == 2 && i == 7)
                grid.Cells[j][i] = Player;
            else
                grid.Cells[j][i] = Way;
        }
    }
    grid.Cells[2][7] = Player;
}

void SetUpPlayer(Game &game)
{
    game.PlayerLocationX = 2;
    game.PlayerLocationY = 7;
}

void SetUpGame(Game &game)
{
    SetUpGrid(game.Grid);
    SetUpPlayer(game);
    game.Status = InProgress;
}

void WriteInstruction(Game game)
{
    if (game.Status == InProgress)
    {
        cout << "You can move ";
        if (game.Grid.Cells[game.PlayerLocationX - 1][game.PlayerLocationY] == Way)
            cout << "W, ";
        if (game.Grid.Cells[game.PlayerLocationX + 1][game.PlayerLocationY] == Way)
            cout << "E, ";
        if (game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY + 1] == Way)
            cout << "S, ";
        if (game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY - 1] == Way)
            cout << "N, ";
    }
    else if (game.Status == Win)
        cout << "Wow - you’ve discovered a large chest filled with GOLD coins!\nYOU WIN! \nThanks for playing. There probably won’t be a next time.";
    else
        cout << "Arrrrgh... you’ve fallen down a pit. \nYOU HAVE DIED! \nThanks for playing. Maybe next time.";
    cout << endl;
}

void MoveThePlayer(Game &game, int toX, int toY)
{
    game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY] = Way;
    game.Grid.Cells[toX][toY] = Player;
    game.PlayerLocationX = toX;
    game.PlayerLocationY = toY;
}

void UpdateWinLose(Game &game, char command)
{
    switch (command)
    {
        case 's':
            if (game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY + 1] == Demise)
                game.Status = Lose;
            if (game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY + 1] == Gold)
                game.Status = Win;
            break;
        case 'n':
            if (game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY - 1] == Demise)
                game.Status = Lose;
            if (game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY - 1] == Gold)
                game.Status = Win;
            break;
        case 'w':
            if (game.Grid.Cells[game.PlayerLocationX - 1][game.PlayerLocationY] == Demise)
                game.Status = Lose;
            if (game.Grid.Cells[game.PlayerLocationX - 1][game.PlayerLocationY] == Gold)
                game.Status = Win;
            break;
        case 'e':
            if (game.Grid.Cells[game.PlayerLocationX + 1][game.PlayerLocationY] == Demise)
                game.Status = Lose;
            if (game.Grid.Cells[game.PlayerLocationX + 1][game.PlayerLocationY] == Gold)
                game.Status = Win;
            break;
    }
}

void UpdateMovement(Game &game, char command)
{
    if (command == 'w' && game.Grid.Cells[game.PlayerLocationX - 1][game.PlayerLocationY] != Wall)
        MoveThePlayer(game, game.PlayerLocationX - 1, game.PlayerLocationY);
    if (command == 'e' && game.Grid.Cells[game.PlayerLocationX + 1][game.PlayerLocationY] != Wall)
        MoveThePlayer(game, game.PlayerLocationX + 1, game.PlayerLocationY);
    if (command == 'n' && game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY - 1] != Wall)
        MoveThePlayer(game, game.PlayerLocationX, game.PlayerLocationY - 1);
    if (command == 's' && game.Grid.Cells[game.PlayerLocationX][game.PlayerLocationY + 1] != Wall)
        MoveThePlayer(game, game.PlayerLocationX, game.PlayerLocationY + 1);
}

void UpdateGame(Game &game, char command)
{
    cout << "Hello" << endl;
    UpdateWinLose(game, command);
    UpdateMovement(game, command);
}

void TakeInput(Game game)
{
    while (command != 'q' && game.Status == InProgress)
    {
        cin >> command;
        gameBlock = false;
    }
        
}

void StartGame(Game &game)
{
    SetUpGame(game);
    thread inputThread(TakeInput, game);
    do
    {
        if (!gameBlock)
        {
            gameBlock = !gameBlock;
            UpdateGame(game, ToLower(command));
            DrawGrid(game.Grid);
            WriteInstruction(game);
        }
        else
        {
            cout << SDL_GetTicks() << endl;
            SDL_Delay(1000);
        }
    } while (ToLower(command) != 'q' && game.Status == InProgress);
    inputThread.join();
}
