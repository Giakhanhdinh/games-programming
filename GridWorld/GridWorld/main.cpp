//
//  main.cpp
//  GridWorld
//
//  Created by Khanh Dinh on 8/7/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include <iostream>
#include "GridWorld.h"
using namespace std;

int main(int argc, const char * argv[])
{
    Game game;
    StartGame(game);
    return 0;
}

