//
//  test_gridworld.cpp
//  GridWorld
//
//  Created by Khanh Dinh on 9/4/17.
//  Copyright © 2017 Khanh Dinh. All rights reserved.
//

#include "GridWorld.h"
#include "catch.hpp"

TEST_CASE("GridWorld Test")
{
    SECTION("Test Cell")
    {
        Cell cell = Wall;
        CHECK(CellChar(cell) == '*');
        cell = Way;
        CHECK(CellChar(cell) == ' ');
        cell = Demise;
        CHECK(CellChar(cell) == 'D');
        cell = Gold;
        CHECK(CellChar(cell) == 'G');
    }
    
    SECTION("Test Grid")
    {
        Grid grid;
        SetUpGrid(grid);
        CHECK(grid.Cells[1][1] == Gold);
        CHECK(grid.Cells[3][1] == Demise);
    }
    
    SECTION("Test Game")
    {
        Game game;
        SetUpGame(game);
        CHECK(game.PlayerLocationX == 2);
        CHECK(game.PlayerLocationY == 7);
        MoveThePlayer(game, 5, 5);
        CHECK(game.PlayerLocationX == 5);
        CHECK(game.PlayerLocationY == 5);
    }
}

